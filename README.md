# Install Akeneo in Debian 9
https://docs.akeneo.com/latest/install_pim/manual/system_requirements/system_requirements.html


# Automated Setup (New Server)
Run this automated one-liner from the directory you want to install your project to:
> macOS & Linux Only

```bash
curl -s https://bitbucket.org/weetek/akeneo-install-debian-9/raw/master/install.sh | bash -s -- pim.projet_name.dev.weetek.net
```

```bash 
DEBIAN_FRONTEND=noninteractive
SERVER_NAME=$1
AKENEO_INSTALL_DIR=/var/www/akeneo
```

# Change locale
```bash
sed -i -e 's/en_US.UTF-8 UTF-8/# en_US.UTF-8 UTF-8/g' /etc/locale.gen
sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen
export LANG=fr_FR.UTF-8
export LANG=fr_FR.UTF-8
export LC_ALL=fr_FR.UTF-8

echo "export LANG=fr_FR.UTF-8
export LANG=fr_FR.UTF-8
export LC_ALL=fr_FR.UTF-8" >> ~/.bashrc


apt update
apt upgrade -y
```

# Installation
## MySQL 5.7
```bash
apt install -y lsb-release apt-transport-https ca-certificates
wget https://dev.mysql.com/get/mysql-apt-config_0.8.12-1_all.deb
apt update
sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password password passwd'
sudo debconf-set-selections <<< 'mysql-server-5.7 mysql-server/root_password_again password passwd'
apt install -y mysql-server
```

## PHP 7.2
```bash
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
echo "deb https://packages.sury.org/php/ stretch main" | sudo tee /etc/apt/sources.list.d/php.list
apt install -y ca-certificates apt-transport-https
apt update
apt install -y \
	php7.2 \
	php7.2-cli \
	php7.2-common \
	php7.2-bcmath \
	php7.2-ctype \
	php7.2-dom \
	php7.2-gd \
	php7.2-iconv \
	php7.2-intl \
	php7.2-mbstring \
	php7.2-mysql \
	php7.2-simplexml \
	php7.2-soap \
	php7.2-xsl \
	php7.2-zip \
	php7.2-xml \
	php7.2-curl \
	php7.2-apcu \
	php7.2-fpm
```

## ELASTICSEARCH 6.5
```bash
apt install -y apt-transport-https
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt update
apt install -y openjdk-8-jre-headless
apt install -y elasticsearch


sysctl -w vm.max_map_count=262144
echo "vm.max_map_count=262144" | tee /etc/sysctl.d/elasticsearch.conf
```

## Apache
```bash
apt install -y apache2
a2enmod rewrite proxy_fcgi
```

## Node
```bash
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt install -y nodejs
```

## Yarn
```bash
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt update && apt install -y yarn
```

## SMPT Agent Postfix
```bash
debconf-set-selections <<< "postfix postfix/mailname string $SERVER_NAME"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'"
apt-get install -y postfix
service postfix start
```

# System configuration

## MySQL
```bash
mysql -e "CREATE DATABASE akeneo_pim;"
mysql -e "GRANT ALL PRIVILEGES ON akeneo_pim.* TO akeneo_pim@localhost IDENTIFIED BY 'akeneo_pim';"
```

## PHP Configs
```bash
sed -i -e 's/;date.timezone =/date.timezone = Etc\/UTC/g' /etc/php/7.2/fpm/php.ini
sed -i -e 's/memory_limit = 128M/memory_limit = 2G/g' /etc/php/7.2/fpm/php.ini
```

## Apache
```bash
cat << EOF >> /etc/apache2/sites-available/akeneo.conf
<VirtualHost *:80>
    ServerName $SERVER_NAME

    DocumentRoot $AKENEO_INSTALL_DIR/web
    <Directory $AKENEO_INSTALL_DIR/web>
        AllowOverride None
        Require all granted

        Options -MultiViews
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^(.*)$ app.php [QSA,L]
    </Directory>

    <Directory $AKENEO_INSTALL_DIR>
        Options FollowSymlinks
    </Directory>

    <Directory $AKENEO_INSTALL_DIR/web/bundles>
        RewriteEngine Off
    </Directory>

    <FilesMatch \.php$>
        SetHandler "proxy:unix:/run/php/php7.2-fpm.sock|fcgi://localhost/"
    </FilesMatch>

    SetEnvIf Authorization .+ HTTP_AUTHORIZATION=$0

    ErrorLog ${APACHE_LOG_DIR}/akeneo-pim_error.log
    LogLevel warn
    CustomLog ${APACHE_LOG_DIR}/akeneo-pim_access.log combined
</VirtualHost>
EOF
```

```bash
a2ensite akeneo
```

# Getting Akeneo PIM
## for icecat version
# wget https://download.akeneo.com/pim-community-standard-v3.1-latest-icecat.tar.gz

## for minimal version
```bash
wget https://download.akeneo.com/pim-community-standard-v3.1-latest.tar.gz
```

#Initializing Akeneo
```bash
mkdir -p $AKENEO_INSTALL_DIR
tar -xvzf pim-community-standard-v3.1-latest.tar.gz -C $AKENEO_INSTALL_DIR
cd $AKENEO_INSTALL_DIR/pim-community-standard
```

## Dependencies
```bash
php -d memory_limit=3G ../composer.phar install --optimize-autoloader --prefer-dist
yarn install
```

## Frontend
```bash
php bin/console cache:clear --no-warmup --env=prod
php bin/console pim:installer:assets --symlink --clean --env=prod
```

## Installation
```bash
bin/console pim:install --force --symlink --clean --env=prod
yarn run webpack
```

## Configuring tasks via crontab
```bash
crontab -l > mycrontab
cat << EOF >> mycrontab
0 23 * * * php $AKENEO_INSTALL_DIR/pim-community-standard/bin/console pim:completeness:calculate --env=prod > $AKENEO_INSTALL_DIR/pim-community-standard/var/logs/calculate_completeness.log 2>&1
0  5 * * * php $AKENEO_INSTALL_DIR/pim-community-standard/bin/console pim:versioning:refresh --env=prod > $AKENEO_INSTALL_DIR/pim-community-standard/var/logs/refresh_versioning.log 2>&1
0 22 * * * php $AKENEO_INSTALL_DIR/pim-community-standard/bin/console pim:volume:aggregate --env=prod > $AKENEO_INSTALL_DIR/pim-community-standard/var/logs/volume_aggregate.log 2>&1
EOF
crontab mycrontab
rm -rf mycrontab
```

# Restart services
```bash
systemctl restart elasticsearch
systemctl restart php7.2-fpm
systemctl restart apache2
```